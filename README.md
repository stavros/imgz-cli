imgz-cli
========

imgz-cli is a command-line utility for IMGZ, the no-bullshit image hosting service. Go to https://imgz.org/ to get an
account, then use this utility to upload images from the safety of your computer.


Installation
------------

```
pip install imgz-cli
```

Done.


Usage
-----

```
imgz /path/to/somefile.jpg
```

Done.


License
-------

MIT.
